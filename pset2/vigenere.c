#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

void encrypt (string keyline, string line);

int main (int argc, string argv[]){
    string keyline;
    string line;
    if(argc==2){
        keyline = argv[1];
        for(int i=0,n=strlen(argv[1]);i<n;i++){
            if(!isalpha(keyline[i])){
                printf("Keyword must be only A-Z and a-z");
                return 1;
            }
        }
        line = GetString();
        encrypt(keyline, line);
        return 0;
    }
    else{
        printf("Incorrect number of arguments, number of arguments for vigenere should be 1");
        return 1;
    }
}

void encrypt (string keyline, string line){
    int j=0,lenght;
    char keychar;
    lenght = strlen(keyline);
    for (int i = 0, n = strlen(line); i < n; i++){
        if(isalpha(line[i])){
            if(isupper(keyline[j]))
                keychar = keyline[j]-65;
            else 
                keychar = keyline[j]-97;
            if(isupper(line[i]))
                line[i] = (((line[i]-65)+keychar)%26)+65;
            else
                line[i] = (((line[i]-97)+keychar)%26)+97;
            j = (j+1)%lenght;
        }
        printf("%c", line[i]);
    }
    printf("\n");
}