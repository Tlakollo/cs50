#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

void encrypt (int key, string line);

int main (int argc, string argv[]){
    int key;
    string line;
    if(argc==2){
        key = atoi(argv[1]);
        line = GetString();
        encrypt(key, line);
        return 0;
    }
    else{
        printf("Yelling at the user!");
        return 1;
    }
}

void encrypt (int key, string line){
    for (int i = 0, n = strlen(line); i < n; i++){
        if(isalpha(line[i])){
            if(isupper(line[i]))
                line[i] = (((line[i]-65)+key)%26)+65;
            else
                line[i] = (((line[i]-97)+key)%26)+97;
        }
        printf("%c", line[i]);
    }
    printf("\n");
}