#include <stdio.h>
#include <cs50.h>
#include <math.h>

int main (void){
    float fmoney;
    int money,coins=0;
    printf("O hai! ");
    do{
        printf("How much change is owed?\n");
        fmoney = GetFloat();
    }while(fmoney<0);
    fmoney*=100;
    money= (int)round(fmoney);
    coins += money/25;
    money%=25;
    coins += money/10;
    money%=10;
    coins += money/5;
    coins += money%5;
    printf("%i\n",coins);
}