#include <stdio.h>
#include <cs50.h>

int main (void){
    int height,i,j;
    do{
        printf("height: ");
        height = GetInt();
    }while(height>23||height<0);
    
    for(i=0;i<height;i++){
        for(j=1;j<height-i;j++){
            printf(" ");
        }
        for(j=0;j<i+2;j++){
            printf("#");
        }
        printf("\n");
    }
}