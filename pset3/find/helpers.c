/**
 * helpers.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Helper functions for Problem Set 3.
 */
       
#include <cs50.h>
#include <stdio.h>
#include "helpers.h"

bool search(int value, int values[], int n);
void sort(int values[], int n);
void swap (int values[], int a, int b);
bool binarysearch(int value, int values[],int start, int end);
void print(int values[],int n);


/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n)
{
    // TODO: implement a searching algorithm
    sort(values, n);
    return binarysearch(value,values,0,n-1);
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    // TODO: implement an O(n^2) sorting algorithm
    int changes;
    int i;
    do{
        changes=0;
        i=n-1;
        while(i){
            if(values[i]<values[i-1]){
                swap(values,i,i-1);
                changes++;
            }
            i--;
        }
    }while(changes>0);
    return;
}
void swap (int values[], int a, int b)
{
    int temp;
    temp = values[a];
    values[a]=values[b];
    values[b]=temp;
}
void print(int values[],int n)
{
    for(int i=0;i<n;i++){
        printf("%i, ",values[i]);
    }
    printf("\n");
}
bool binarysearch(int value, int values[],int start, int end)
{
    int half;
    if(end-start<2){
        if (values[start]==value||values[end]==value)
            return true;
        else
            return false;
    }
    half=(end+start)/2;
    if(values[half]==value){
        return true;
    }
    else if (values[half]<value){
        return binarysearch(value,values,half,end);
    }
    else{
        return binarysearch(value,values,start,half);
    }
}